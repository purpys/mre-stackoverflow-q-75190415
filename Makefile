DOCKER_COMPOSE_FILE = ./docker/docker-compose.yml

all: up

up:
	docker compose -f $(DOCKER_COMPOSE_FILE) up -d --build

down:
	docker compose -f $(DOCKER_COMPOSE_FILE) down

sclean:
	docker system prune -a

re: down up

sre: down sclean up

# Usage: make log C=<container_name>
log: C=
log:
	docker logs $(C)

# Usage: make exec C=<container_name>
exec: C=
exec:
	docker container exec -ti $(C)bash