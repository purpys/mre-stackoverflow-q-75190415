#!/bin/bash

function exit_error() {
	printf "[ERROR]:\t%s\n" "${@}" >&2
	exit 1
}

set -ex

echo "Executing Starting Point:"

if [[ -z "${WORKDIR}" ]]; then
	exit_error "'${WORKDIR}' is not set"
fi

if [[ ! -d "${WORKDIR}node_modules" ]]; then
	exit_error "${WORKDIR}node_modules does not exist"
fi

exec $@