FROM node:18 AS base

# Set in docker-compose.yml-file
ARG WORKDIR
ARG TOOLSDIR
ARG LOCALDIR
ENV WORKDIR=${WORKDIR}

# Install dumb-init for the init system
RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.5/dumb-init_1.2.5_x86_64
RUN chmod +x /usr/local/bin/dumb-init

WORKDIR ${WORKDIR}
RUN mkdir -p ${WORKDIR}

# Copy package.json to the current workdir (for npm install)
COPY ${LOCALDIR}/package*.json ${WORKDIR}

# Install all Packages (refereed from package.json)
RUN npm install

COPY ${TOOLSDIR}/* /usr/local/bin/
COPY ${LOCALDIR}/src/* ${WORKDIR}

# Frontend Stage
FROM base AS frontend

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD [ "/usr/local/bin/start.sh", "ng", "serve" ]

# Backend Stage
FROM base AS backend

EXPOSE 3000

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD [ "/usr/local/bin/start.sh", "npm", "run", "start:dev" ]